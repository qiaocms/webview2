
/* settings 可用参数 https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.web.webview2.core.corewebview2settings?view=webview2-dotnet-1.0.1150.38
AreBrowserAcceleratorKeysEnabled
确定是否启用特定于浏览器的快捷键。

AreDefaultContextMenusEnabled
确定是否在 Web 视图中向用户显示默认上下文菜单。

AreDefaultScriptDialogsEnabled
确定 WebView 是否呈现默认的"Javascript"对话框。

AreDevToolsEnabled
确定用户是否能够使用上下文菜单或键盘快捷方式打开 DevTools 窗口。

AreHostObjectsAllowed
确定是否可从 WebView 中的页面访问宿主对象。

IsBuiltInErrorPageEnabled
确定是否禁用导航失败和呈现进程失败的内置错误页。

IsGeneralAutofillEnabled
确定是否保存和自动填充常规表单信息。

IsPasswordAutosaveEnabled
确定是否将自动保存密码信息。

IsPinchZoomEnabled
确定最终用户在启用了触摸输入的设备上使用捏合运动在 WebView2 中缩放 Web 内容的能力。

IsScriptEnabled
确定是否在 WebView 的所有将来导航中启用运行 JavaScript。

IsStatusBarEnabled
确定是否显示状态栏。

IsSwipeNavigationEnabled
确定最终用户是否在启用了触摸输入的设备上使用轻扫手势在 WebView2 中导航。

IsWebMessageEnabled
确定是否允许从主机到 Web 视图的顶级 HTML 文档进行通信。
IsZoomControlEnabled
确定用户是否能够影响 Web 视图的缩放。

UserAgent
确定 WebView2 的用户代理。
*/

/*

All	0
Specifies all resources.

CspViolationReport	15
Specifies a CSP Violation Report.

Document	1
Specifies a document resources.

EventSource	10
Specifies an EventSource API communication.

Fetch	8
Specifies a Fetch API communication.

Font	5
Specifies a font resource.

Image	3
Specifies an image resources.

Manifest	12
Specifies a Web App Manifest.

Media	4
Specifies another media resource such as a video.

Other	16
Specifies an other resource.

Ping	14
Specifies a Ping request.

Script	6
Specifies a script resource.

SignedExchange	13
Specifies a Signed HTTP Exchange.

Stylesheet	2
Specifies a CSS resources.

TextTrack	9
Specifies a TextTrack resource.

Websocket	11
Specifies a WebSocket API communication.

XmlHttpRequest	7
Specifies an XML HTTP request.

*/